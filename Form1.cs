﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5_THI
{
    public partial class Form1 : Form
    {
        List<Vehicle> listOfVechicles = new List<Vehicle>();
        List<PartsOfRouts> listOfPartsOfRoute = new List<PartsOfRouts>();
        List<Route> listOFRouts = new List<Route>();
        string radioButtonState="radioButton1";
        public Form1()
        {
            InitializeComponent();
        }

        public void VehiclesAdd(Vehicle vehicle)
        {
            listOfVechicles.Add(vehicle);
            UpdateList(radioButtonState);
        }

        public void PartOfRouteAdd(PartsOfRouts part)
        {
            listOfPartsOfRoute.Add(part);
            UpdateList(radioButtonState);
        }

        public void RouteAdd(Route route)
        {
            listOFRouts.Add(route);
            UpdateList(radioButtonState);
        }

        public List<PartsOfRouts> GetPartOfRoute()
        {
            return listOfPartsOfRoute;
        }

        public List<Vehicle> GetVehicles()
        {
            return listOfVechicles;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            RouteConstructor.CreateVehicles(listOfVechicles);
            RouteConstructor.CreateParts(listOfPartsOfRoute, listOfVechicles);
            RouteConstructor.CreateRoute(listOFRouts, listOfPartsOfRoute);
            for (int i = 0; i < listOFRouts.Count; i++)
            {
                listBox1.Items.Add(listOFRouts[i].Name);
            }
            listBox1.SelectedIndex = 0;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton EventButton = (RadioButton)sender;
            string name = EventButton.Name;
            UpdateList(name);
        }

        private void CreateButtonPressed(object sender, EventArgs e)
        {
            Button EventButton = (Button)sender;
            switch (EventButton.Name)
            {
                case "button1":
                    Form2 newForm2 = new Form2();
                    newForm2.mainWindow = this;
                    newForm2.ShowDialog();
                    break;
                case "button2":
                    Form4 newForm4 = new Form4();
                    newForm4.mainWindow = this;
                    newForm4.Show();
                    break;
                case "button3":
                    Form3 newForm3 = new Form3();
                    newForm3.mainWindow = this;
                    newForm3.Show();
                    break;
                default:
                    break;
            }
        }

        private void DeleteButtonPressed(object sender, EventArgs e)
        {
            if (CheckContaimentInAnother() == true)
            {
                if (listBox1.SelectedIndex >= 0)
                {
                    switch (radioButtonState)
                    {
                        case "radioButton1":
                            listOFRouts.Remove(listOFRouts[listBox1.SelectedIndex]);
                            break;
                        case "radioButton2":
                            listOfPartsOfRoute.Remove(listOfPartsOfRoute[listBox1.SelectedIndex]);
                            break;
                        case "radioButton3":
                            listOfVechicles.Remove(listOfVechicles[listBox1.SelectedIndex]);
                            break;
                        default:
                            break;
                    }
                }
                UpdateList(radioButtonState);
            }
            else
                MessageBox.Show("Ошибка: данный элемент используется при построении маршрута!");
        }

        private Boolean CheckContaimentInAnother()
        {
            if (radioButtonState == "radioButton2")
            {
                for (int i = 0; i < listOFRouts.Count; i++)
                {
                    for (int j=0; j<listOFRouts[i].GetRouteList().Count; j++)
                    {
                        if (listOFRouts[i].GetRouteList()[j] == listOfPartsOfRoute[listBox1.SelectedIndex])
                            return false;
                    }
                }
            }
            else if (radioButtonState == "radioButton3")
            {
                for (int i = 0; i <listOfPartsOfRoute.Count; i++)
                {
                    if (listOfPartsOfRoute[i].VechicleName == listOfVechicles[listBox1.SelectedIndex])
                        return false;
                }
            }
            return true;
        }

        private void UpdateList(String name)
        {
            switch (name)
            {
                case "radioButton1":
                    listBox1.Items.Clear();
                    for (int i = 0; i < listOFRouts.Count; i++)
                    {
                        listBox1.Items.Add(listOFRouts[i].Name);
                    }
                    break;
                case "radioButton2":
                    listBox1.Items.Clear();
                    for (int i = 0; i < listOfPartsOfRoute.Count; i++)
                    {
                        listBox1.Items.Add(listOfPartsOfRoute[i].StartPoint + "-" + listOfPartsOfRoute[i].Destination);
                    }
                    break;
                case "radioButton3":
                    listBox1.Items.Clear();
                    for (int i = 0; i < listOfVechicles.Count; i++)
                    {
                        listBox1.Items.Add(listOfVechicles[i].GetName());
                    }
                    break;
                default:
                    break;
            }
            //listBox1.SelectedIndex = 0;
            textBox1.Clear();
            radioButtonState = name;
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox ElementChecked = (ListBox)sender;
            textBox1.Clear();
            if (ElementChecked.Items.Count>=0)
            {
                if (ElementChecked.SelectedIndex>=0)
                {
                    switch (radioButtonState)
                    {
                        case "radioButton1":
                            textBox1.Text = listOFRouts[ElementChecked.SelectedIndex].Name + ":" + Environment.NewLine;
                            for (int i = 0; i < listOFRouts[ElementChecked.SelectedIndex].GetRouteList().Count; i++)
                            {
                                textBox1.AppendText(listOFRouts[ElementChecked.SelectedIndex].GetRouteList()[i].StartPoint + " - " + listOFRouts[ElementChecked.SelectedIndex].GetRouteList()[i].Destination + Environment.NewLine);
                            }
                            textBox1.AppendText("TOTAL COST: " + Math.Round(listOFRouts[ElementChecked.SelectedIndex].GetRouteCost(), 2) + " y.e.");
                            break;
                        case "radioButton2":
                            textBox1.Text = "From: " + listOfPartsOfRoute[ElementChecked.SelectedIndex].StartPoint + Environment.NewLine;
                            textBox1.AppendText("To: " + listOfPartsOfRoute[ElementChecked.SelectedIndex].Destination + Environment.NewLine);
                            textBox1.AppendText("Length: " + listOfPartsOfRoute[ElementChecked.SelectedIndex].Length + Environment.NewLine);
                            textBox1.AppendText("Vehicle: " + listOfPartsOfRoute[ElementChecked.SelectedIndex].VechicleName.GetName() + Environment.NewLine);
                            break;
                        case "radioButton3":
                            textBox1.Text = "Vehicle name: " + listOfVechicles[ElementChecked.SelectedIndex].GetName() + Environment.NewLine;
                            textBox1.AppendText("Type: " + listOfVechicles[ElementChecked.SelectedIndex].GetType().ToString() + Environment.NewLine);
                            textBox1.AppendText("Comfort Factor: " + listOfVechicles[ElementChecked.SelectedIndex].GetComfort() + Environment.NewLine);
                            textBox1.AppendText("Cost Per kilometer: " + Math.Round(listOfVechicles[ElementChecked.SelectedIndex].GetCostPerKm(), 2) + " y.e." + Environment.NewLine);
                            break;
                        default:
                            break;
                    }
                }
               
            }
            
        }
    }
}
