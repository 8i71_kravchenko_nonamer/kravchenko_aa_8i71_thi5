﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5_THI
{
    public partial class Form4 : Form
    {
        public Form1 mainWindow;
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            for (int i=0; i<mainWindow.GetVehicles().Count; i++)
            {
                comboBox1.Items.Add(mainWindow.GetVehicles()[i].GetName());
            }
            comboBox1.SelectedIndex = 0;
        }

        private void CreationButtonPressed(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                if (double.TryParse(textBox3.Text, out double n) == true)
                {
                    PartsOfRouts part = new PartsOfRouts();
                    part.FillRotePart(textBox1.Text, textBox2.Text, mainWindow.GetVehicles()[comboBox1.SelectedIndex], Convert.ToDouble(textBox3.Text));
                    mainWindow.PartOfRouteAdd(part);
                    MessageBox.Show("Отрезок создан!");
                }
                else
                    MessageBox.Show("Расстояние введено неверно!");
            }
            else
                MessageBox.Show("Не все поля заполнены!");
        }
    }
}
