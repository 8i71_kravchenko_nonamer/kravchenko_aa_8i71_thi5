﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5_THI
{
    public partial class Form3 : Form
    {
        public Form1 mainWindow;
        Route route = new Route();
        public Form3()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && listBox1.Items.Count > 0)
            {
                route.Name = textBox1.Text;
                mainWindow.RouteAdd(route);
                MessageBox.Show("Маршрут создан!");
            }
            else
                MessageBox.Show("Не заполнено имя и/или нет путевых точек!");
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox1.Items.Clear();
            for (int i = 0; i < mainWindow.GetPartOfRoute().Count; i++)
            {
                listBox2.Items.Add(mainWindow.GetPartOfRoute()[i].StartPoint + " - " + mainWindow.GetPartOfRoute()[i].Destination);
            }
            listBox2.SelectedIndex = 0;
        }

        private void AddItem(object sender, EventArgs e)
        {
            route.AddRouteWaipoint(mainWindow.GetPartOfRoute()[listBox2.SelectedIndex]);
            listBox1.Items.Add(mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].StartPoint + " - " + mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].Destination);
        }

        private void DelItem(object sender, EventArgs e)
        {
            if (listBox1.Items.Count>0)
            {
                route.DelPartOfRoute(route.GetRouteList()[listBox1.SelectedIndex]);
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }

        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox ElementChecked = (ListBox)sender;
            textBox2.Clear();
            if (ElementChecked.Items.Count >= 0)
            {
                textBox2.Text = "From: " + mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].StartPoint + Environment.NewLine;
                textBox2.AppendText("To: " + mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].Destination + Environment.NewLine);
                textBox2.AppendText("Length: " + mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].Length + Environment.NewLine);
                textBox2.AppendText("Vehicle: " + mainWindow.GetPartOfRoute()[listBox2.SelectedIndex].VechicleName.GetName() + Environment.NewLine);
            }
        }

    }
}

