﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5_THI
{
    public class RouteConstructor
    {
        static public void CreateVehicles(List<Vehicle> vehicles)
        {
            Auto vehicle = new Auto();
            vehicle.SetQualityOfRoad(5);
            vehicle.SetName("Audi TT");
            vehicle.SetAutoClass("Business Class");
            vehicles.Add(vehicle);

            vehicle = new Auto();
            vehicle.SetAutoClass("Business Class");
            vehicle.SetQualityOfRoad(4);
            vehicle.SetName("BMW");
            vehicles.Add(vehicle);

            Train train = new Train();
            train.SetName("TEE");
            train.SetQualityOfRoad(1);
            train.SetTypeOfSeat("Coupe");
            vehicles.Add(train);

            SeaVehicle ship = new SeaVehicle();
            ship.SetName("Lason");
            ship.SetClassOfSeaVehicle("First Class");
            vehicles.Add(ship);

            AirVehicle airplane = new AirVehicle();
            airplane.SetName("B777");
            airplane.SetClassOfSeat("First Class");
            vehicles.Add(airplane);
        }
        static public void CreateParts(List<PartsOfRouts> parts, List<Vehicle> vehicles)
        {
            PartsOfRouts routePart = new PartsOfRouts();
            routePart.FillRotePart("Berlin", "London", vehicles[0], 200.0);
            parts.Add(routePart);

            routePart = new PartsOfRouts();
            routePart.FillRotePart("London", "Paris", vehicles[1], 400.0);
            parts.Add(routePart);

            routePart = new PartsOfRouts();
            routePart.FillRotePart("Paris", "Rome", vehicles[2], 800.0);
            parts.Add(routePart);

            routePart = new PartsOfRouts();
            routePart.FillRotePart("Rome", "Venice", vehicles[3], 500.0);
            parts.Add(routePart);

            routePart = new PartsOfRouts();
            routePart.FillRotePart("Venice", "Beijing", vehicles[4], 6000.0);
            parts.Add(routePart);
        }
        static public void CreateRoute(List<Route> listOFRouts, List<PartsOfRouts> listOfPartsOfRoute)
        {
            Route route = new Route();
            route.Name = "Route from Berlin to Beijing";
            route.AddRouteWaipoint(listOfPartsOfRoute[0]);
            route.AddRouteWaipoint(listOfPartsOfRoute[1]);
            route.AddRouteWaipoint(listOfPartsOfRoute[2]);
            route.AddRouteWaipoint(listOfPartsOfRoute[3]);
            route.AddRouteWaipoint(listOfPartsOfRoute[4]);
            listOFRouts.Add(route);
        }

        static public void CorrectRoute(Route route, PartsOfRouts Part, List<Vehicle> listOfVechicles)
        {
            route.DelPartOfRoute(Part);
            route.Name = "Test Route after correction";
            route.GetRouteList()[1].VechicleName = listOfVechicles[0];
            route.Name = "Route from Berlin to Venice";
        }

    }

}
