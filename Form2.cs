﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5_THI
{
    public partial class Form2 : Form
    {
        String[] vehicleTypes = new String[] { "Автомобиль", "Автобус", "Поезд", "Морской", "Воздушный" };
        String[] airSeatTypes = new String[] { "Business Class", "First Class", "Economic Class" };
        String[] seaSeatTypes = new String[] { "First Class", "Second Class", "Economic Class" };
        String[] autoClassTypes = new String[] { "Lux Class", "Business Class", "Standard Class" };
        String[] busClassTypes = new String[] { "City Bus", "Intercity Bus" };
        String[] trainClassTypes = new String[] { "Coupe", "Public" };
        public Form1 mainWindow;
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void ComboBox1_click(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            for (int i = 0; i < vehicleTypes.Count(); i++)
            {
                comboBox1.Items.Add(vehicleTypes[i]);
            }
        }

        private void ComboBox1_Checked(object sender, EventArgs e)
        {
            label2.Text = "Название:";
            label2.Visible = true;
            textBox1.Visible = true;
            comboBox2.Visible = true;
            label3.Visible = true;
            trackBar1.Value = 10;
            ComboBox CheckedComboBox = (ComboBox)sender;
            switch (CheckedComboBox.SelectedIndex)
            {
                case 0:
                    label3.Text = "Класс авто:";
                    comboBox2.Items.Clear();
                    for (int i = 0; i < autoClassTypes.Count(); i++)
                        comboBox2.Items.Add(autoClassTypes[i]);
                    comboBox2.SelectedIndex = 0;
                    label4.Text = "Качество дороги";
                    label4.Visible = true;
                    trackBar1.Visible = true;
                    break;
                case 1:
                    label3.Text = "Тип автобуса:";
                    comboBox2.Items.Clear();

                    for (int i = 0; i < busClassTypes.Count(); i++)
                        comboBox2.Items.Add(busClassTypes[i]);
                    comboBox2.SelectedIndex = 0;
                    label4.Text = "Качество дороги";
                    label4.Visible = true;
                    trackBar1.Visible = true;
                    break;
                case 2:
                    label3.Text = "Тип места";
                    comboBox2.Items.Clear();
                    for (int i = 0; i < trainClassTypes.Count(); i++)
                        comboBox2.Items.Add(trainClassTypes[i]);
                    comboBox2.SelectedIndex = 0;
                    label4.Text = "Качество путей";
                    label4.Visible = true;
                    trackBar1.Visible = true;
                    break;
                case 3:
                    label3.Text = "Класс:";
                    comboBox2.Items.Clear();
                    for (int i = 0; i < seaSeatTypes.Count(); i++)
                        comboBox2.Items.Add(seaSeatTypes[i]);
                    comboBox2.SelectedIndex = 0;
                    label4.Visible = false;
                    trackBar1.Visible = false;
                    break;
                case 4:
                    label3.Text = "Тип места:";
                    comboBox2.Items.Clear();
                    for (int i = 0; i < airSeatTypes.Count(); i++)
                        comboBox2.Items.Add(airSeatTypes[i]);
                    comboBox2.SelectedIndex = 0;
                    label4.Visible = false;
                    trackBar1.Visible = false;
                    break;
                default:
                    break;
            }
        }

        private void CreationButtonPressed(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                switch (comboBox1.SelectedIndex)
                {
                    case 0:
                        Auto auto = new Auto();
                        auto.SetQualityOfRoad(trackBar1.Value);
                        auto.SetName(textBox1.Text);
                        auto.SetAutoClass(comboBox1.SelectedText);
                        mainWindow.VehiclesAdd(auto);
                        break;
                    case 1:
                        Bus bus = new Bus();
                        bus.SetQualityOfRoad(Convert.ToDouble(trackBar1.Value));
                        bus.SetName(textBox1.Text);
                        bus.SetBusClass(comboBox1.SelectedText);
                        mainWindow.VehiclesAdd(bus);
                        break;
                    case 2:
                        Train train = new Train();
                        train.SetQualityOfRoad(trackBar1.Value);
                        train.SetName(textBox1.Text);
                        train.SetTypeOfSeat(comboBox1.SelectedText);
                        mainWindow.VehiclesAdd(train);
                        break;
                    case 3:
                        SeaVehicle ship = new SeaVehicle();
                        ship.SetName(textBox1.Text);
                        ship.SetClassOfSeaVehicle(comboBox1.SelectedText);
                        mainWindow.VehiclesAdd(ship);
                        break;
                    case 4:
                        AirVehicle airplane = new AirVehicle();
                        airplane.SetName(textBox1.Text);
                        airplane.SetClassOfSeat(comboBox1.SelectedText);
                        mainWindow.VehiclesAdd(airplane);
                        break;
                    default:
                        break;
                }
                MessageBox.Show("Транспорт создан!");
            }
            else
                MessageBox.Show("Не все поля заполнены!");
           
        }

    }
}
